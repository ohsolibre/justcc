CC = gcc
CFLAGS = -Wall -Wextra
LDFLAGS = -static

jcc: jcc.c elf.c x86_64.c opcode.c
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $^

clean:
	rm -f jcc
