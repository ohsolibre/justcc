#include <err.h>
#include <elf.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#include "jcc.h"

static Elf64_Sym *sym;
static Elf64_Rel *textrel, *datrel;
static char *text, *symstr, *dat;
static size_t text_n, textrel_n, sym_n, symstr_n, dat_n, datrel_n, bss_n;

void elf_sym(char *name, long shndx, long size, long bind)
{
	size_t i, len = strlen(name) + 1;

	for (i = 0; i < sym_n; i++)
		if (!strcmp(name, symstr + sym[i].st_name))
			return;

	sym = realloc(sym, (sym_n + 1) * sizeof(*sym));
	sym[sym_n].st_name = symstr_n;
	sym[sym_n].st_shndx = shndx;
	sym[sym_n].st_size = size;
	sym[sym_n].st_info = ELF64_ST_INFO(bind, (shndx == SEC_NULL || shndx == SEC_TEXT) ?
	                                         STT_FUNC : STT_OBJECT);
	sym[sym_n++].st_value = shndx == SEC_TEXT ? text_n :
	                        shndx == SEC_DAT ? dat_n :
	                        shndx == SEC_BSS ? bss_n : 0;

	symstr = realloc(symstr, symstr_n + len);
	strcpy(symstr + symstr_n, name);
	symstr_n += len;
}

void elf_datrel(long idx, long offset, long type)
{
	datrel = realloc(datrel, (datrel_n + 1) * sizeof(*datrel));
	datrel[datrel_n].r_offset = offset;
	datrel[datrel_n++].r_info = ELF64_R_INFO(idx, type);
}

void elf_textrel(long idx, long offset, long type)
{
	textrel = realloc(textrel, (textrel_n + 1) * sizeof(*textrel));
	textrel[textrel_n].r_offset = offset;
	textrel[textrel_n++].r_info = ELF64_R_INFO(idx, type);
}

void elf_text(void *buf, size_t len)
{
	text = realloc(text, text_n + len);
	memcpy(text + text_n, buf, len);
	text_n += len;
}

void elf_dat(void *buf, size_t len)
{
	dat = realloc(dat, dat_n);
	memcpy(dat + dat_n, buf, len);
	dat_n += len;
}

void elf_write(char *filename)
{
	int fd;
	Elf64_Ehdr h = {0};
	Elf64_Shdr s[SEC_N] = {0};

	h.e_ident[0] = 0x7f;
	h.e_ident[1] = 'E';
	h.e_ident[2] = 'L';
	h.e_ident[3] = 'F';
	h.e_ident[4] = ELFCLASS64;
	h.e_ident[5] = ELFDATA2LSB;
	h.e_ident[6] = EV_CURRENT;
	h.e_type = ET_REL;
	h.e_machine = EM_X86_64;
	h.e_version = EV_CURRENT;
	h.e_ehsize = sizeof(h);
	h.e_shentsize = sizeof(*s);
	h.e_shoff = sizeof(h);
	h.e_shnum = SEC_N;
	h.e_shstrndx = SEC_STR;

	s[SEC_TEXT].sh_type = SHT_PROGBITS;
	s[SEC_TEXT].sh_offset = sizeof(h) + sizeof(s);
	s[SEC_TEXT].sh_size = text_n;
	s[SEC_TEXT].sh_entsize = 1;
	s[SEC_TEXT].sh_flags = SHF_EXECINSTR | SHF_ALLOC;
	s[SEC_TEXT].sh_addralign = 16;

	s[SEC_REL].sh_type = SHT_REL;
	s[SEC_REL].sh_offset = s[SEC_TEXT].sh_offset + s[SEC_TEXT].sh_size;
	s[SEC_REL].sh_size = textrel_n * sizeof(*textrel);
	s[SEC_REL].sh_entsize = sizeof(*textrel);
	s[SEC_REL].sh_link = SEC_SYM;
	s[SEC_REL].sh_info = SEC_TEXT;

	s[SEC_SYM].sh_type = SHT_SYMTAB;
	s[SEC_SYM].sh_offset = s[SEC_REL].sh_offset + s[SEC_REL].sh_size;
	s[SEC_SYM].sh_size = sym_n * sizeof(*sym);
	s[SEC_SYM].sh_entsize = sizeof(*sym);
	s[SEC_SYM].sh_link = SEC_STR;
	s[SEC_SYM].sh_info = 1;

	s[SEC_STR].sh_type = SHT_STRTAB;
	s[SEC_STR].sh_offset = s[SEC_SYM].sh_offset + s[SEC_SYM].sh_size;
	s[SEC_STR].sh_size = symstr_n;
	s[SEC_STR].sh_entsize = 1;

	s[SEC_DAT].sh_type = SHT_PROGBITS;
	s[SEC_DAT].sh_offset = s[SEC_STR].sh_offset + s[SEC_STR].sh_size;
	s[SEC_DAT].sh_size = dat_n;
	s[SEC_DAT].sh_entsize = 1;
	s[SEC_DAT].sh_flags = SHF_ALLOC | SHF_WRITE;
	s[SEC_DAT].sh_addralign = 16;

	s[SEC_DATREL].sh_type = SHT_REL;
	s[SEC_DATREL].sh_offset = s[SEC_DAT].sh_offset + s[SEC_DAT].sh_size;
	s[SEC_DATREL].sh_size = datrel_n * sizeof(*datrel);
	s[SEC_DATREL].sh_entsize = sizeof(*datrel);
	s[SEC_DATREL].sh_link = SEC_SYM;
	s[SEC_DATREL].sh_info = SEC_DAT;

	s[SEC_BSS].sh_type = SHT_NOBITS;
	s[SEC_BSS].sh_offset = s[SEC_DATREL].sh_offset + s[SEC_DATREL].sh_size;
	s[SEC_BSS].sh_size = bss_n;
	s[SEC_BSS].sh_entsize = 1;
	s[SEC_BSS].sh_flags = SHF_ALLOC | SHF_WRITE;
	s[SEC_BSS].sh_addralign = 16;

	if ((fd = open(filename, O_WRONLY | O_TRUNC | O_CREAT, 0600)) < 0)
		err(1, "open");
	write(fd, &h, sizeof(h));
	write(fd, &s, sizeof(s));
	write(fd, text, text_n);
	write(fd, textrel, sizeof(*textrel) * textrel_n);
	write(fd, sym, sizeof(*sym) * sym_n);
	write(fd, symstr, symstr_n);
	write(fd, dat, sizeof(*dat) * dat_n);
	write(fd, datrel, sizeof(*datrel) * datrel_n);
	close(fd);
}
