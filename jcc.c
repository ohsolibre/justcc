#define _GNU_SOURCE
#include <err.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "jcc.h"

#define T_STATIC (1 << 0)
#define T_EXTERN (1 << 1)
#define T_SIGNED (1 << 2)
#define T_ARRAY  (1 << 3)
#define T_FUNC   (1 << 4)
#define T_STRUCT (1 << 5)

#define MIN(a, b)   ((a) < (b) ? (a) : (b))
#define ALIGN(x, a) (((x) + (a) - 1) & ~((a) - 1))
#define T_SIZE(t) ((t)->ptr ? SIZEOF_LONG : (t)->size)
#define isident(x) (isalpha(x) || (x) == '_')

#define LIST_DATA(l) (void *) ((char *) (l) + sizeof(*(l)))
struct list {
	char *name;
	struct list *next;
	/* char data[0]; */
};

struct type {
	uint size;
	uint flags;
	uint ptr;
};

struct var {
	struct type type;
	long addr;
};

struct func {
	struct type ret;
	struct list *args;
};

struct struc {
	struct list *fields;
	uint size;
};

struct macro {
	char *def;
	struct list *args;
};

/* if name or size is 0, then it is a macro expansion */
struct file {
	char *name;
	char *map;
	char *ptr;
	size_t size;
	struct file *prev;
};

static char **inc_path;
static int inc_path_count;

static struct file *file;
static char *tok;
static size_t toklen;
static struct list *types, *structs, *unions, *funcs, *enums, *globals, *locals;
static struct list *macros, *labels;

static void cpp_read(void);
static void expr_read(void);
static long expr_const(void);
static void expr_comma(void);
static void expr_prefix(void);
static void stmt_read(void);
static void func_def(char *name, struct type t);
static void struct_def(struct type *t, int isunion);

static void file_add(char *name, char *s, size_t size)
{
	struct file *f = malloc(sizeof(*f));
	f->name = name;
	f->map = f->ptr = s;
	f->size = size;
	f->prev = file;
	file = f;
}

static int file_open(char *name)
{
	int fd, flags;
	char *s;
	struct stat st;

	if ((fd = open(name, O_RDONLY)) < 0)
		return -1;
	if (fstat(fd, &st) < 0)
		return close(fd), -1;
	flags = MAP_PRIVATE | (st.st_size ? 0 : MAP_ANONYMOUS);
	s = mmap(NULL, st.st_size + 1, PROT_READ, flags, fd, 0);
	close(fd);
	if (s == MAP_FAILED)
		return -1;
	file_add(strdup(name), s, st.st_size + 1);
	return 0;
}

static void file_close(void)
{
	struct file *f = file->prev;
	if (file->size)
		munmap(file->map, file->size);
	else
		free(file->map);
	free(file->name);
	free(file);
	file = f;
}

static void die(char *fmt, ...)
{
	char *p;
	uint line;
	va_list ap;

	while (!file->name) /* skip macro expansions */
		file_close();
	for (line = 1, p = file->map; p < file->ptr; line++, p++)
		if (!(p = memchr(p, '\n', file->ptr - p)))
			break;

	fprintf(stderr, "%s:%u: ", file->name, line);
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	exit(1);
}

static char *esc_char(char *s, long *n)
{
	char c, *p, *chr = "abefnrvt", *esc = "\a\b\e\f\n\r\v\t";

	if (s[0] != '\\')
		return *n = s[0], s + 1;
	else if ((p = strchr(chr, s[1])) != NULL)
		return *n = esc[p - chr], s + 2;
	else if (s[1] >= '0' && s[1] <= '7')
		for (*n = 0, s += 1; *s >= '0' && *s <= '7'; s++)
			*n = *n * 8 + *s - '0';
	else if (s[1] == 'x')
		for (*n = 0, s += 2; isxdigit((c = tolower(*s))); s++)
			*n = *n * 16 + c - (c <= '9' ? '0' : 'a' - 10);
	else
		return *n = s[1], s + 2;
	return s;
}

static char *skip_space(char *s)
{
	for (;;) {
		if (isspace(*s))
			s++;
		else if (s[0] == '\\' && s[1] == '\n')
			s += 2;
		else if (s[0] == '/' && s[1] == '/')
			s = strchrnul(s, '\n');
		else if (s[0] == '/' && s[1] == '*') {
			while ((s = strchr(s + 1, '/')) && *(s - 1) != '*');
			if (!s++)
				die("unterminated comment\n");
		} else
			return s;
	}
}

static void tok_read(void)
{
	static char *tokens[] = {
		"<<=", ">>=", "...", "<<", ">>", "++", "--", "+=", "-=", "*=", "/=",
		"%=", "|=", "&=", "^=", "&&", "||", "==", "!=", "<=", ">=", "->", NULL
	};
	char c, **t, *s = file->ptr = tok = skip_space(file->ptr);

	if (isident(*s))
		for (s++; isident(*s) || isdigit(*s); s++);
	else if (isdigit(*s))
		for (s++; isxdigit(*s) || memchr("lux", tolower(*s), 3); s++);
	else if (*s == '"' || *s == '\'') { 
		for (c = *s++; *s && *s != c && *s != '\n'; s++)
			if (*s == '\\')
				s++;
		if (*s++ != c)
			die("missing terminating %c character\n", c);
	} else if (*s) {
		for (t = tokens; *t && strncmp(s, *t, strlen(*t)); t++);
		s += *t ? strlen(*t) : 1;
	} else if (file->prev)
		return file_close(), tok_read();

	file->ptr = s;
	toklen = s - tok;
	cpp_read();
}

static char *tok_dup(void)
{
	char *s = strndup(tok, toklen);
	return tok_read(), s;
}

static int tok_jmp(char *s)
{
	if (toklen == strlen(s) && !memcmp(s, tok, toklen))
		return tok_read(), 0;
	return -1;
}

static void tok_expect(char *s)
{
	if (tok_jmp(s))
		die("expected '%s' got '%.*s'\n", s, toklen, tok);
}

static long tok_num(struct type *t)
{
	long val = 0;
	char c, *s = tok;

	if (s[0] == '0' && tolower(s[1]) == 'x')
		for (s += 2; isxdigit((c = tolower(*s))); s++)
			val = val * 16 + c - (c <= '9' ? '0' : 'a' - 10);
	else if (s[0] == '0')
		for (s += 1; *s >= '0' && *s <= '7'; s++)
			val = val * 8 + *s - '0';
	else if (isdigit(s[0]))
		for (; *s >= '0' && *s <= '9'; s++)
			val = val * 10 + *s - '0';
	else if (s[0] == '\'' && s[1] != '\'')
		s = esc_char(&s[1], &val) + 1;

	t->size = 4;
	t->flags = T_SIGNED;
	t->ptr = 0;
	for (; s < tok + toklen; s++) {
		if (tolower(*s) == 'l')
			t->size = SIZEOF_LONG;
		else if (tolower(*s) == 'u')
			t->flags &= ~T_SIGNED;
		else
			die("syntax error `%.*s`\n", toklen, tok);
	}
	return tok_read(), val;
}

static char *tok_str(void)
{
	long n;
	size_t len = 0;
	char *p, *s = NULL;

	while (p = tok, *p++ == '"') {
		s = realloc(s, len + toklen - 1);
		while (p < tok + toklen - 1) {
			p = esc_char(p, &n);
			s[len++] = (char) n;
		}
		s[len] = '\0';
		tok_read();
	}
	return s;
}

static void list_free(struct list **head, struct list *until)
{
	struct list *l, *next;
	for (l = *head; l != until; l = next) {
		next = l->next;
		free(l->name);
		free(l);
	}
	*head = l;
}

static void *list_find(struct list **head, char *name)
{
	struct list *l;
	for (l = *head; l; l = l->next)
		if (l->name && (name ? !strcmp(l->name, name) : !tok_jmp(l->name)))
			return LIST_DATA(l);
	return NULL;
}

static void list_add(struct list **head, char *name, void *data, size_t size)
{
	struct list *l = malloc(sizeof(*l) + size);
	l->name = name;
	l->next = *head;
	memcpy(LIST_DATA(l), data, size);
	if (head != &funcs && head != &globals && list_find(head, name))
		die("'%s' redeclared\n", name);
	*head = l;
}

static void array_read(struct type *t)
{
	long n;
	if (!tok_jmp("[")) {
		n = expr_const();
		tok_expect("]");
		array_read(t);

		t->flags |= T_ARRAY;
		t->size = n + 1 * T_SIZE(t);
		t->ptr = 0;
	}
}

static char *name_read(struct type *t)
{
	struct type t1 = {0};
	char *p = NULL, *name = NULL;

	while (!tok_jmp("*"))
		t->ptr++;
	if (!tok_jmp("(")) {
		p = name = name_read(&t1);
		tok_expect(")");
	} else if (isident(*tok))
		name = tok_dup();
	if (!tok_jmp("(")) {
		func_def(strdup(name), *t);
		tok_expect(")");
		t->ptr = 0;
		t->flags |= T_FUNC;
	} else
		array_read(t);

	if (p) {
		t->ptr = t1.ptr;
		t->flags |= t1.flags;
	}
	return name;
}

static void type_def(char *name, struct type *type)
{
	list_add(&types, name, type, sizeof(*type));
}

static int type_find(struct type *type)
{
	struct type *t = (struct type *) list_find(&types, NULL);
	if (t == NULL)
		return -1;
	type->ptr |= t->ptr;
	type->flags |= t->flags;
	type->size = t->size;
	return 0;
}

static void enum_def(void)
{
	int n;
	char *name;

	if (isident(*tok))
		tok_read();
	if (tok_jmp("{"))
		return;
	for (n = 0; isident(*tok); n++) {
		name = tok_dup();
		if (!tok_jmp("="))
			n = expr_const();

		list_add(&enums, name, &n, sizeof(n));
		if (tok_jmp(","))
			break;
	}
	tok_expect("}");
}

static int type_read(struct type *t)
{
	int i;

	t->size = 4;
	t->flags = T_SIGNED;
	t->ptr = 0;
	for (i = -1;; i = 0) {
		if (!tok_jmp("void"))
			t->size = 0;
		else if (!tok_jmp("char"))
			t->size = 1;
		else if (!tok_jmp("short"))
			t->size = 2;
		else if (!tok_jmp("long"))
			t->size = SIZEOF_LONG;
		else if (!tok_jmp("signed"))
			t->flags |= T_SIGNED;
		else if (!tok_jmp("unsigned"))
			t->flags &= ~T_SIGNED;
		else if (!tok_jmp("static"))
			t->flags |= T_STATIC;
		else if (!tok_jmp("extern"))
			t->flags |= T_EXTERN;
		else if (!tok_jmp("struct"))
			return struct_def(t, 0), 0;
		else if (!tok_jmp("union"))
			return struct_def(t, 1), 0;
		else if (!tok_jmp("enum"))
			return enum_def(), 0;
		else if (!tok_jmp("int") || !tok_jmp("auto") ||
			 !tok_jmp("register") || !tok_jmp("volatile") ||
			 !tok_jmp("inline") || !tok_jmp("const")) {}
		else if (isident(*tok) && !type_find(t))
			return 0;
		else
			return i;
	}
}

static int def_read(void (*func)(), void *data)
{
	char *name;
	struct type type, t;

	if (type_read(&type))
		return -1;
	for (t = type; (name = name_read(&t)); t = type) {
		if (!(t.flags & T_FUNC) && !t.size && !t.ptr)
			die("storage size of '%s' is not known\n", name);
		if (!(t.flags & T_FUNC) || t.ptr)
			func(name, &t, data);
		if (tok_jmp(","))
			return 0;
	}
	return -1;
}

static void global_def(char *name, struct type *t)
{
	long val = 0;
	struct var v = {.type = *t};

	list_add(&globals, name, &v, sizeof(v));
	if (!tok_jmp("=")) {
		if (t->flags & T_EXTERN)
			elf_sym(name, SEC_NULL, 0, STB_GLOBAL);
		else
			elf_sym(name, SEC_DAT, T_SIZE(t), STB_GLOBAL);
		val = expr_const();
	} else
		elf_sym(name, SEC_BSS, T_SIZE(t), STB_GLOBAL);
	printf("<%u> %s = %li\n", T_SIZE(t), name, val);
}

static void local_def(char *name, struct type *t)
{
	long val = 0;
	struct var v = {.type = *t};

	list_add(&locals, name, &v, sizeof(v));
	if (!tok_jmp("="))
		val = expr_const();
	printf("<%u> %s = %li\n", T_SIZE(t), name, val);
}

static void func_def(char *name, struct type t)
{
	struct var v;
	struct func f = {.ret = t};

	while (tok_jmp("...") && !type_read(&t)) {
		v.type = t;
		list_add(&f.args, name_read(&v.type), &v, sizeof(v));
		if (tok_jmp(","))
			break;
	}
	list_add(&funcs, name, &f, sizeof(f));
}

static void struct_field_def(char *name, struct type *t, struct struc *s)
{
	struct var v = {.type = *t};
	list_add(&s->fields, name, &v, sizeof(v));
}

static void struct_def(struct type *t, int isunion)
{
	uint size, padding = 0;
	char *name;
	struct var *v;
	struct struc *p, s = {0};
	struct list *l, **head = isunion ? &unions : &structs;

	if (isident(*tok) && (p = list_find(head, NULL)) && p->size) {
		s = *p;
		goto out;
	}

	name = isident(*tok) ? tok_dup() : NULL;
	if (tok_jmp("{"))
		goto out;
	while (tok_jmp("}"))
		def_read(struct_field_def, &s), tok_expect(";");

	for (l = s.fields; l; l = l->next) {
		v = LIST_DATA(l);
		size = T_SIZE(&v->type);
		if (padding < size)
			padding = size;
		if (!isunion) {
			v->addr = ALIGN(s.size, MIN(SIZEOF_LONG, size));
			s.size = v->addr + size;
		} else if (s.size < size)
			s.size = size;
	}
	s.size = ALIGN(s.size, MIN(SIZEOF_LONG, padding));
	list_add(head, name, &s, sizeof(s));
out:
	t->size = s.size;
	t->flags |= T_STRUCT;
}

static void expr_binary(int op)
{
	if (op < 0)
		return;
	o_binary(op);
}

static void expr_call(void)
{
	if (tok_jmp(")")) {
		do {
			expr_read();
		} while (!tok_jmp(","));
		tok_expect(")");
	}
}

static void expr_postfix(void)
{
	while (1) {
		if (!tok_jmp("[")) {
			expr_comma();
			tok_expect("]");
		} else if (!tok_jmp("(")) {
			expr_call();
		} else if (!tok_jmp("++")) {
		} else if (!tok_jmp("--")) {
		} else if (!tok_jmp(".")) {
		} else if (!tok_jmp("->")) {
		} else break;
	}
}

static void expr_primary(void)
{
	struct type t;

	if (isident(*tok)) {
		if (list_find(&locals, NULL)) {
		} else if (list_find(&globals, NULL)) {
		} else if (list_find(&enums, NULL)) {
		} else if (list_find(&funcs, NULL)) {
		} else
			die("'%.*s' undeclared\n", toklen, tok);
	} else if (*tok == '"') {
		free(tok_str());
	} else if (!tok_jmp("(")) {
		if (!type_read(&t)) {
			if (name_read(&t))
				die("unexpected identifier before ')'\n");
			tok_expect(")");
			expr_prefix();
		} else {
			expr_comma();
			tok_expect(")");
		}
	} else if (isxdigit(*tok) || *tok == '\'') {
		o_mov(O_NUM, tok_num(&t));
	} else
		die("expected expression before '%.*s'\n", toklen, tok);
	expr_postfix();
}

static void expr_prefix(void)
{
	int n;
	struct type t;

	while (1) {
		if (!tok_jmp("sizeof")) {
			n = tok_jmp("(");
			if (type_read(&t))
				n ? expr_comma() : expr_prefix();
			else if (name_read(&t))
				die("unexpected identifier before ')'\n");
			if (!n) tok_expect(")");
			return;
		} else if (!tok_jmp("&")) {
		} else if (!tok_jmp("*")) {
		} else if (!tok_jmp("!")) {
		} else if (!tok_jmp("+")) {
		} else if (!tok_jmp("-")) {
		} else if (!tok_jmp("~")) {
		} else if (!tok_jmp("++")) {
		} else if (!tok_jmp("--")) {
		} else break;
	}
	expr_primary();
}

static void expr_mul(void)
{
	int n = -1;
	while (expr_prefix(), 1) {
		expr_binary(n);
		if      (!tok_jmp("*")) n = O_MUL;
		else if (!tok_jmp("/")) n = O_DIV;
		else if (!tok_jmp("%")) n = O_MOD;
		else break;
	}
}

static void expr_add(void)
{
	int n = -1;
	while (expr_mul(), 1) {
		expr_binary(n);
		if      (!tok_jmp("+")) n = O_ADD;
		else if (!tok_jmp("-")) n = O_SUB;
		else break;
	}
}

static void expr_shift(void)
{
	int n = -1;
	while (expr_add(), 1) {
		expr_binary(n);
		if      (!tok_jmp("<<")) n = O_SHL;
		else if (!tok_jmp(">>")) n = O_SHR;
		else break;
	}
}

static void expr_cmp(void)
{
	int n = -1;
	while (expr_shift(), 1) {
		expr_binary(n);
		if      (!tok_jmp("<"))  n = O_LT;
		else if (!tok_jmp(">"))  n = O_GT;
		else if (!tok_jmp("<=")) n = O_LE;
		else if (!tok_jmp(">=")) n = O_GE;
		else break;
	}
}

static void expr_eq(void)
{
	int n = -1;
	while (expr_cmp(), 1) {
		expr_binary(n);
		if      (!tok_jmp("==")) n = O_EQ;
		else if (!tok_jmp("!=")) n = O_NE;
		else break;
	}
}

static void expr_bitand(void)
{
	expr_eq();
	while (!tok_jmp("&")) {
		expr_eq();
		expr_binary(O_AND);
	}
}

static void expr_xor(void)
{
	expr_bitand();
	while (!tok_jmp("^")) {
		expr_bitand();
		expr_binary(O_XOR);
	}
}

static void expr_bitor(void)
{
	expr_xor();
	while (!tok_jmp("|")) {
		expr_xor();
		expr_binary(O_OR);
	}
}

static void expr_and(void)
{
	while (expr_bitor(), !tok_jmp("&&"));
}

static void expr_or(void)
{
	while (expr_and(), !tok_jmp("||"));
}

static void expr_cond(void)
{
	expr_or();
	if (tok_jmp("?"))
		return;
	expr_cond();
	tok_expect(":");
	expr_cond();
}

static long expr_const(void)
{
	long n;
	expr_cond();
	if (o_popnum(&n))
		die("expected constant expression\n");
	return n;
}

static void expr_read(void)
{
	for (expr_cond();; expr_read()) {
		if      (!tok_jmp("="));
		else if (!tok_jmp("+="));
		else if (!tok_jmp("-="));
		else if (!tok_jmp("*="));
		else if (!tok_jmp("%="));
		else if (!tok_jmp("&="));
		else if (!tok_jmp("|="));
		else if (!tok_jmp("^="));
		else if (!tok_jmp("<<="));
		else if (!tok_jmp(">>="));
		else break;
	}
}

static void expr_comma(void)
{
	while (expr_read(), !tok_jmp(","));
}

static void stmt_switch(void)
{
	tok_expect("(");
	expr_comma();
	tok_expect(")");
	stmt_read();
}

static void stmt_if(void)
{
	tok_expect("(");
	expr_comma();
	tok_expect(")");
	stmt_read();
	if (!tok_jmp("else"))
		stmt_read();
}

static void stmt_while(void)
{
	tok_expect("(");
	expr_comma();
	tok_expect(")");
	stmt_read();
}

static void stmt_do(void)
{
	stmt_read();
	tok_expect("while");
	tok_expect("(");
	expr_comma();
	tok_expect(")");
	tok_expect(";");
}

static void stmt_for(void)
{
	tok_expect("(");
	if (tok_jmp(";"))
		expr_comma(), tok_expect(";");
	if (tok_jmp(";"))
		expr_comma(), tok_expect(";");
	if (tok_jmp(")"))
		expr_comma(), tok_expect(")");
	stmt_read();
}

static void stmt_read(void)
{
	if (!tok_jmp("{")) {
		struct list *_types = types,
			    *_funcs = funcs,
			    *_enums = enums,
			    *_locals = locals,
			    *_unions = unions,
			    *_structs = structs,
			    *_globals = globals;
		while (tok_jmp("}"))
			stmt_read();
		list_free(&types, _types);
		list_free(&funcs, _funcs);
		list_free(&enums, _enums);
		list_free(&locals, _locals);
		list_free(&unions, _unions);
		list_free(&structs, _structs);
		list_free(&globals, _globals);
	} else if (!tok_jmp("if")) {
		stmt_if();
	} else if (!tok_jmp("while")) {
		stmt_while();
	} else if (!tok_jmp("do")) {
		stmt_do();
	} else if (!tok_jmp("for")) {
		stmt_for();
	} else if (!tok_jmp("switch")) {
		stmt_switch();
	} else if (!tok_jmp("return")) {
		if (tok_jmp(";"))
			expr_comma(), tok_expect(";");
	} else if (!tok_jmp("break")) {
		tok_expect(";");
	} else if (!tok_jmp("continue")) {
		tok_expect(";");
	} else if (!tok_jmp("goto")) {
		tok_expect(";");
	} else if (!tok_jmp("typedef")) {
		def_read(type_def, NULL), tok_expect(";");
	} else if (!def_read(local_def, NULL)) {
		tok_expect(";");
	} else if (isident(*tok) && *skip_space(file->ptr) == ':') {
		list_add(&labels, tok_dup(), NULL, 0);
		tok_expect(":");
	} else if (tok_jmp(";"))
		expr_comma(), tok_expect(";");
}

static void func_read(void)
{
	struct list *l;
	struct func *f = LIST_DATA(funcs);

	if (*tok != '{' || list_find(&globals, funcs->name))
		return tok_expect(";");
	for (l = f->args; l && l->name; l = l->next)
		list_add(&locals, strdup(l->name), LIST_DATA(l), sizeof(struct var));

	elf_sym(funcs->name, SEC_TEXT, 0, STB_GLOBAL);
	i_func_beg();
	stmt_read();
	i_func_end();
	list_free(&labels, NULL);
	list_free(&locals, NULL);
}

static void cpp_include(char *eol)
{
	int i, ret = -1;
	char path[PATH_MAX], file[256], *s = NULL;

	if (eol > tok && (*tok == '"' || *tok == '<'))
		s = memchr(tok + 1, *tok == '"' ? '"' : '>', eol - tok);
	if (s == NULL)
		die("#include expects \"FILE\" or <FILE>\n");

	snprintf(file, sizeof(file), "%.*s", (int)(s - tok - 1), tok + 1);
	if (*tok == '"')
		ret = file_open(file);
	if (ret) {
		for (i = 0; i < inc_path_count && ret; i++) {
			snprintf(path, sizeof(path), "%s/%s", inc_path[i], file);
			ret = file_open(path);
		}
		if (ret)
			die("cannot include '%s': %m\n", file);
	}
	tok_read();
}

static void cpp_define(char *eol)
{
	char *name, *p;
	struct macro m = {0};

	if (eol < tok || !isident(*tok))
		die("expected identifier after #define\n");
	p = tok + toklen;
	name = tok_dup();

	if (*p == '(' && !tok_jmp("(")) {
		while (eol > tok && isident(*tok)) {
			list_add(&m.args, tok_dup(), NULL, 0);
			if (eol < tok || tok_jmp(","))
				break;
		}
		if (eol < tok || tok_jmp(")"))
			die("expected ')'\n");
	}
	m.def = strndup(tok, eol - tok);
	list_add(&macros, name, &m, sizeof(m));
}

static char *cpp_eol(void)
{
	char *s, *p = tok;

	if (tok_jmp("#"))
		return NULL;

	for (s = p - 1; s > file->map && *s != '\n'; s--)
		if (!isspace(*s))
			die("stray '#' in program\n");

	for (p++; *p && *p != '\n'; p++)
		if (*p == '\\')
			p++;
	return p;
}

static int cpp_expr(void)
{
	return expr_const();
}

static void cpp_jumpif(int jump_else)
{
	char *p;
	int loop = 1, depth = 0;

	while (loop) {
		if (!*tok)
			die("unterminated #if\n");

		if ((p = cpp_eol()) == NULL)
			tok_read();
		else if (!tok_jmp("if") || !tok_jmp("ifdef") || !tok_jmp("ifndef"))
			depth++;
		else if (!tok_jmp("endif") && !depth--)
			loop = 0;
		else if (!tok_jmp("else") && !jump_else && !depth)
			loop = 0;
		else if (!tok_jmp("elif") && !jump_else && !depth && cpp_expr())
			loop = 0;

		while (tok < p)
			tok_read();
	}
}

static void cpp_ifdef(char *eol, int not)
{
	if (eol < tok || !isident(*tok))
		die("expected identifier after #ifdef\n");
	if (!list_find(&macros, NULL) == !not)
		cpp_jumpif(0);
}

static void cpp_if(void)
{
	if (!cpp_expr())
		cpp_jumpif(0);
}

static void cpp_read(void)
{
	static char *p; /* EOL of current directive */

	if (*tok != '#' || p != NULL)
		return;
	while ((p = cpp_eol()) != NULL) {

		if (tok > p) {
			continue;
		} else if (!tok_jmp("if")) {
			cpp_if();
		} else if (!tok_jmp("ifdef")) {
			cpp_ifdef(p, 0);
		} else if (!tok_jmp("ifndef")) {
			cpp_ifdef(p, 1);
		} else if (!tok_jmp("elif")) {
			cpp_jumpif(1);
		} else if (!tok_jmp("else")) {
			cpp_jumpif(1);
		} else if (!tok_jmp("endif")) {
		} else if (!tok_jmp("include")) {
			file->ptr = p;
			cpp_include(p);
			continue;
		} else if (!tok_jmp("define")) {
			cpp_define(p);
		} else if (!tok_jmp("undef")) {
		} else
			die("unknown directive\n");

		while (tok < p)
			tok_read();
	}
	p = NULL;
}

int main(int argc, char **argv)
{
	int i;
	char *objname, *name = NULL;

	for (i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			name = argv[i];
		} else if (argv[i][1] == 'I') {
			inc_path = realloc(inc_path, sizeof(*inc_path) * (inc_path_count + 1));
			inc_path[inc_path_count++] = argv[i][2] ? &argv[i][2] : argv[++i];
		}
	}

	if (!name)
		errx(1, "usage: %s [-I inc] <file>", argv[0]);
	if (file_open(name))
		err(1, "cannot open '%s'", name);

	elf_sym("", SEC_NULL, 0, STB_GLOBAL);
	tok_read();
	while (*tok) {
		if (!tok_jmp("typedef"))
			def_read(type_def, NULL), tok_expect(";");
		else if (!def_read(global_def, NULL) && funcs)
			func_read();
		else
			tok_expect(";");
	}
	file_close();
	o_dump();
	return 0;

	objname = basename(name);
	objname[strlen(objname)-1] = 'o';
	elf_write(objname);
}
