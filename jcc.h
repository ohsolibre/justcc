#ifndef _JCC_H
#define _JCC_H

#include <elf.h>
#include <stddef.h>

#define SEC_NULL   0
#define SEC_TEXT   1
#define SEC_REL    2
#define SEC_SYM    3
#define SEC_STR    4
#define SEC_DAT    5
#define SEC_DATREL 6
#define SEC_BSS    7
#define SEC_N      8

/* operand type */
enum {O_REG, O_NUM, O_LOC, O_SYM}; /* register, immediate, local variable, symbol */

/* instruction opcodes */
enum {O_MOV, O_ADD, O_SUB, O_AND, O_OR, O_XOR, O_SHL, O_SHR,
      O_MUL, O_DIV, O_MOD, O_EQ, O_NE, O_GE, O_LE, O_LT, O_GT,
      O_NEG, O_NOT, O_LNOT, O_RET, O_LDR, O_STR};

void o_mov(int type, long arg);
void o_binary(int code);
void o_unary(int code);
void o_dump(void);
int o_popnum(long *n);

void elf_sym(char *name, long shndx, long size, long bind);
void elf_datrel(long idx, long offset, long type);
void elf_textrel(long idx, long offset, long type);
void elf_text(void *buf, size_t len);
void elf_dat(void *buf, size_t len);
void elf_write(char *filename);

void i_func_beg(void);
void i_func_end(void);

extern unsigned int SIZEOF_LONG;

#endif /* _JCC_H */
