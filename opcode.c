#include <stdio.h>
#include <stdlib.h>

#include "jcc.h"

struct opcode {
	uint8_t code; /* O_ADD, O_SUB, ... */
	uint8_t type; /* O_REG, O_NUM, O_LOC, O_SYM */
	uint8_t size; /* operand size */
	uint8_t sign; /* is signed? */
	long arg[3];
};

static struct opcode *op; /* stack of instructions */
static size_t op_count, op_size;

static void op_put(struct opcode *p)
{
	if (op_count == op_size) {
		op_size = op_size ? op_size * 2 : 128;
		op = realloc(op, sizeof(*op) * op_size);
	}
	op[op_count++] = *p;
}

static int op_getnum(long n, long *arg)
{
	struct opcode *o = op + n;
	if (o->code == O_MOV && o->type == O_NUM)
		return *arg = o->arg[0], 0;
	return -1;
}

static int compute_binary(int code)
{
	long a, b, c;

	if (op_getnum(op_count - 2, &a) || op_getnum(op_count - 1, &b))
		return -1;

	if (b == 0 && (code == O_DIV || code == O_MOD))
		return -1; /* division by zero */

	switch (code) {
	case O_ADD: c = a + b; break;
	case O_SUB: c = a - b; break;
	case O_AND: c = a & b; break;
	case O_OR : c = a | b; break;
	case O_XOR: c = a ^ b; break;
	case O_SHL: c = a << b; break;
	case O_SHR: c = a >> b; break;
	case O_MUL: c = a * b; break;
	case O_DIV: c = a / b; break;
	case O_MOD: c = a % b; break;
	case O_EQ : c = a == b; break;
	case O_NE : c = a != b; break;
	case O_GE : c = a >= b; break;
	case O_LE : c = a <= b; break;
	case O_LT : c = a < b; break;
	case O_GT : c = a > b; break;
	}

	op_count -= 2;
	o_mov(O_NUM, c);
	return 0;
}

int o_popnum(long *n)
{
	if (!op_getnum(op_count - 1, n))
		return op_count--, 0;
	return -1;
}

void o_mov(int type, long arg)
{
	struct opcode o = {O_MOV, type, 0, 0, {arg, 0, 0}};
	op_put(&o);
}

void o_binary(int code)
{
	struct opcode o = {code, O_REG, 0, 0, {op_count-2, op_count-1, 0}};

	if (compute_binary(code))
		op_put(&o);
}

void o_unary(int code)
{
	struct opcode o = {code, O_REG, 0, 0, {op_count-1, 0, 0}};
	op_put(&o);
}

void o_dump(void)
{
	static char *types[] = {"reg", "num", "loc", "sym"};
	static char *codes[] = {
		"MOV", "ADD", "SUB", "AND", "OR", "XOR", "SHL", "SHR", "MUL",
		"DIV", "MOD", "EQ", "NE", "GE", "LE", "LT", "GT",
		"NEG", "NOT", "LNOT", "RET", "LDR", "STR"};
	size_t i;
	struct opcode *p;

	for (i = 0; i < op_count; i++) {
		p = op + i;
		printf("%s %s %lu %lu\n", codes[p->code], types[p->type], p->arg[0], p->arg[1]);
	}
}
