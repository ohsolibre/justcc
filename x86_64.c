#include "jcc.h"

unsigned int SIZEOF_LONG = 8;

void i_func_beg(void)
{
	elf_text("\x55" "\x48\x89\xe5", 4); /* push %rbp; mov %rsp, %rbp */
}

void i_func_end(void)
{
	elf_text("\xc9" "\xc3", 2); /* leave; ret */
}
